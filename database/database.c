#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <sys/stat.h>

void createDatabase(char *name) {
    // Periksa validitas nama database (hanya angka dan huruf)
    for (int i = 0; i < strlen(name); i++) {
        if (!((name[i] >= 'a' && name[i] <= 'z') || (name[i] >= 'A' && name[i] <= 'Z') || (name[i] >= '0' && name[i] <= '9'))) {
            printf("Nama database tidak valid. Gunakan hanya huruf dan angka.\n");
            return;
        }
    }

    // Buat direktori dengan nama database
    char dirname[256];
    snprintf(dirname, sizeof(dirname), "databaseku/%s", name);
    int status = mkdir(dirname, 0777);

    if (status == 0) {
        printf("Database '%s' berhasil dibuat.\n", name);
    } else {
        printf("Gagal membuat database '%s'.\n", name);
    }
}

void createTable(char *databaseName, char *tableName, char *columns) {
    // Periksa validitas nama tabel (hanya angka dan huruf)
    for (int i = 0; i < strlen(tableName); i++) {
        if (!((tableName[i] >= 'a' && tableName[i] <= 'z') || (tableName[i] >= 'A' && tableName[i] <= 'Z') || (tableName[i] >= '0' && tableName[i] <= '9'))) {
            printf("Nama tabel tidak valid. Gunakan hanya huruf dan angka.\n");
            return;
        }
    }

    // Buat path file tabel dalam database
    char filePath[256];
    snprintf(filePath, sizeof(filePath), "databaseku/%s/%s.csv", databaseName, tableName);

    // Periksa apakah database sudah ada
    char databasePath[256];
    snprintf(databasePath, sizeof(databasePath), "databaseku/%s", databaseName);
    struct stat st;
    if (stat(databasePath, &st) != 0 || !S_ISDIR(st.st_mode)) {
        printf("Database '%s' tidak ditemukan.\n", databaseName);
        return;
    }

    // Buka file untuk ditulis
    FILE *file = fopen(filePath, "w");
    if (file == NULL) {
        printf("Gagal membuat tabel '%s'.\n", tableName);
        return;
    }
    
    // Hapus tanda ")" pada judul kolom
    int len = strlen(columns);
    columns[len - 2] = '\0';
    
    // Tulis header file csv dengan nama kolom saja
    fprintf(file, "%s\n", columns);
    
    fclose(file);

    printf("Tabel '%s' berhasil dibuat pada database '%s'.\n", tableName, databaseName);
}

void dropDatabase(char *name) {
    // Buat path folder database
    char dirname[256];
    snprintf(dirname, sizeof(dirname), "databaseku/%s", name);

    // Hapus folder database
    int status = rmdir(dirname);

    if (status == 0) {
        printf("Database '%s' berhasil dihapus.\n", name);
    } else {
        printf("Gagal menghapus database '%s'.\n", name);
    }
}

void dropTable(char *databaseName, char *tableName) {
    // Buat path file tabel dalam database
    char filePath[256];
    snprintf(filePath, sizeof(filePath), "databaseku/%s/%s.csv", databaseName, tableName);

    // Periksa apakah tabel sudah ada
    if (access(filePath, F_OK) == -1) {
        printf("Tabel '%s' tidak ditemukan pada database '%s'.\n", tableName, databaseName);
        return;
    }

    // Hapus file tabel
    int status = remove(filePath);

    if (status == 0) {
        printf("Tabel '%s' berhasil dihapus dari database '%s'.\n", tableName, databaseName);
    } else {
        printf("Gagal menghapus tabel '%s' dari database '%s'.\n", tableName, databaseName);
    }
}

void dropColumn(char *databaseName, char *tableName, char *columnName) {
    // Buat path file tabel dalam database
    char filePath[256];
    snprintf(filePath, sizeof(filePath), "databaseku/%s/%s.csv", databaseName, tableName);

    // Periksa apakah tabel sudah ada
    if (access(filePath, F_OK) == -1) {
        printf("Tabel '%s' tidak ditemukan pada database '%s'.\n", tableName, databaseName);
        return;
    }

    // Buka file tabel untuk dibaca
    FILE *file = fopen(filePath, "r");
    if (file == NULL) {
        printf("Gagal membuka tabel '%s'.\n", tableName);
        return;
    }

    // Baca baris pertama (header)
    char line[200];
    fgets(line, sizeof(line), file);

    // Cari indeks kolom yang akan dihapus
    int columnIndex = -1;
    char *token = strtok(line, ",");
    int index = 0;
    while (token != NULL) {
        if (strcmp(token, columnName) == 0) {
            columnIndex = index;
            break;
        }
        index++;
        token = strtok(NULL, ",");
    }

    fclose(file);

    // Jika kolom tidak ditemukan
    if (columnIndex == -1) {
        printf("Kolom '%s' tidak ditemukan dalam tabel '%s' pada database '%s'.\n", columnName, tableName, databaseName);
        return;
    }

    // Buat file temporary untuk menulis hasil penghapusan kolom
    char tempFilePath[256];
    snprintf(tempFilePath, sizeof(tempFilePath), "databaseku/%s/temp.csv", databaseName);
    FILE *tempFile = fopen(tempFilePath, "w");
    if (tempFile == NULL) {
        printf("Gagal membuat file temporary.\n");
        return;
    }

    // Buka file tabel asli untuk dibaca
    file = fopen(filePath, "r");
    if (file == NULL) {
        printf("Gagal membuka tabel '%s'.\n", tableName);
        fclose(tempFile);
        remove(tempFilePath);
        return;
    }

    // Baca baris-baris selanjutnya dan tulis ke file temporary tanpa kolom yang dihapus
    while (fgets(line, sizeof(line), file)) {
        char *token = strtok(line, ",");
        int columnCount = 0;
        while (token != NULL) {
            columnCount++;
            if (columnCount != columnIndex + 1) {
                fprintf(tempFile, "%s,", token);
            }
            token = strtok(NULL, ",");
        }
        fprintf(tempFile, "\n");
    }

    fclose(file);
    fclose(tempFile);

    // Hapus file tabel asli
    remove(filePath);

    // Rename file temporary menjadi file tabel asli
    rename(tempFilePath, filePath);

    printf("Kolom '%s' berhasil dihapus dari tabel '%s' pada database '%s'.\n", columnName, tableName, databaseName);
}


void insertRow(char *databaseName, char *tableName, char *values) {
    // Buat path file tabel dalam database
    char filePath[256];
    snprintf(filePath, sizeof(filePath), "databaseku/%s/%s.csv", databaseName, tableName);

    // Periksa apakah tabel sudah ada
    if (access(filePath, F_OK) == -1) {
        printf("Tabel '%s' tidak ditemukan pada database '%s'.\n", tableName, databaseName);
        return;
    }

    // Buka file tabel untuk ditulis (mode "a" agar data ditambahkan di akhir file)
    FILE *file = fopen(filePath, "a");
    if (file == NULL) {
        printf("Gagal membuka tabel '%s'.\n", tableName);
        return;
    }

    // Pisahkan nilai-nilai pada perintah INSERT
    char *token = strtok(values, ",");
    char line[200] = "";
    bool firstValue = true;

    // Tulis nilai-nilai ke file tabel
    while (token != NULL) {
        // Hapus karakter spasi di awal dan akhir nilai
        while (*token == ' ' || *token == '(' || *token == '\'')
            token++;
        size_t len = strlen(token);
        while (len > 0 && (token[len - 1] == ' ' || token[len - 1] == ')' || token[len - 1] == '\''))
            token[--len] = '\0';

        // Tulis nilai ke baris
        if (firstValue) {
            firstValue = false;
            snprintf(line + strlen(line), sizeof(line) - strlen(line), "%s", token);
        } else {
            snprintf(line + strlen(line), sizeof(line) - strlen(line), ",%s", token);
        }

        token = strtok(NULL, ",");
    }

    fprintf(file, "%s\n", line);
    fclose(file);

    printf("Data berhasil ditambahkan pada tabel '%s' di database '%s'.\n", tableName, databaseName);
}

void updateColumnValue(char *databaseName, char *tableName, char *columnName, char *newValue) {
    // Buat path file tabel dalam database
    char filePath[256];
    snprintf(filePath, sizeof(filePath), "databaseku/%s/%s.csv", databaseName, tableName);

    // Periksa apakah tabel sudah ada
    if (access(filePath, F_OK) == -1) {
        printf("Tabel '%s' tidak ditemukan pada database '%s'.\n", tableName, databaseName);
        return;
    }

    // Buka file tabel untuk dibaca
    FILE *file = fopen(filePath, "r");
    if (file == NULL) {
        printf("Gagal membuka tabel '%s'.\n", tableName);
        return;
    }

    // Baca baris pertama (header)
    char line[200];
    fgets(line, sizeof(line), file);

    // Cari indeks kolom yang akan diupdate
    int columnIndex = -1;
    char *token = strtok(line, ",");
    int index = 0;
    while (token != NULL) {
        if (strcmp(token, columnName) == 0) {
            columnIndex = index;
            break;
        }
        index++;
        token = strtok(NULL, ",");
    }

    fclose(file);

    // Jika kolom tidak ditemukan
    if (columnIndex == -1) {
        printf("Kolom '%s' tidak ditemukan dalam tabel '%s' pada database '%s'.\n", columnName, tableName, databaseName);
        return;
    }

    // Buat file temporary untuk menulis hasil update kolom
    char tempFilePath[256];
    snprintf(tempFilePath, sizeof(tempFilePath), "databaseku/%s/temp.csv", databaseName);
    FILE *tempFile = fopen(tempFilePath, "w");
    if (tempFile == NULL) {
        printf("Gagal membuat file temporary.\n");
        return;
    }

    // Buka file tabel asli untuk dibaca
    file = fopen(filePath, "r");
    if (file == NULL) {
        printf("Gagal membuka tabel '%s'.\n", tableName);
        fclose(tempFile);
        remove(tempFilePath);
        return;
    }

    // Baca baris pertama (header) dan tulis ke file temporary
    fgets(line, sizeof(line), file);
    fprintf(tempFile, "%s", line);

    // Baca baris-baris selanjutnya dan tulis ke file temporary dengan kolom yang diupdate
    while (fgets(line, sizeof(line), file)) {
        char *token = strtok(line, ",");
        int columnCount = 0;
        while (token != NULL) {
            columnCount++;
            if (columnCount == columnIndex + 1) {
                fprintf(tempFile, "%s,", newValue);
            } else {
                fprintf(tempFile, "%s,", token);
            }
            token = strtok(NULL, ",");
        }
        fprintf(tempFile, "\n");
    }

    fclose(file);
    fclose(tempFile);

    // Hapus file tabel asli
    remove(filePath);

    // Ganti nama file temporary menjadi file tabel asli
    rename(tempFilePath, filePath);

    printf("Kolom '%s' pada tabel '%s' dalam database '%s' berhasil diupdate.\n", columnName, tableName, databaseName);
}

void deleteRows(char *databaseName, char *tableName) {
    // Create the file path for the table within the database
    char filePath[256];
    snprintf(filePath, sizeof(filePath), "databaseku/%s/%s.csv", databaseName, tableName);

    // Check if the table already exists
    if (access(filePath, F_OK) == -1) {
        printf("Table '%s' not found in database '%s'.\n", tableName, databaseName);
        return;
    }

    // Open the table file for reading
    FILE *file = fopen(filePath, "r");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", tableName);
        return;
    }

    // Read the first line (header)
    char line[200];
    fgets(line, sizeof(line), file);
    
    fclose(file);

    // Open the table file for writing (this will delete all the rows)
    file = fopen(filePath, "w");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", tableName);
        return;
    }

    // Write the header back into the file
    fprintf(file, "%s", line);
    
    fclose(file);

    printf("All rows have been deleted from table '%s' in database '%s'.\n", tableName, databaseName);
}

void selectAll(char *databaseName, char *tableName, int new_socket) {
    // Buat path file tabel dalam database
    char filePath[256];
    snprintf(filePath, sizeof(filePath), "databaseku/%s/%s.csv", databaseName, tableName);

    // Periksa apakah tabel sudah ada
    if (access(filePath, F_OK) == -1) {
        printf("Tabel '%s' tidak ditemukan pada database '%s'.\n", tableName, databaseName);
        return;
    }

    // Buka file tabel untuk dibaca
    FILE *file = fopen(filePath, "r");
    if (file == NULL) {
        printf("Gagal membuka tabel '%s'.\n", tableName);
        return;
    }

    // Baca dan tampilkan seluruh isi file
    char line[200];
    while (fgets(line, sizeof(line), file)) {
        printf("%s", line);
        send(new_socket, line, strlen(line), 0);
    }
   

    fclose(file);
}

void displayColumnData(char *databaseName, char *tableName, char *columnName) {
    // Buat path file tabel dalam database
    char filePath[256], cols_arr[10][100];
    char *tmp_token;
    tmp_token = strtok(columnName, ", ");
    int cols_idx = 0;
    while (tmp_token != NULL) {
        strcpy(cols_arr[cols_idx], tmp_token);
        cols_idx++;
        tmp_token = strtok(NULL, ", ");
    }
    snprintf(filePath, sizeof(filePath), "databaseku/%s/%s.csv", databaseName,
             tableName);

    // Periksa apakah tabel sudah ada
    if (access(filePath, F_OK) == -1) {
        printf("Tabel '%s' tidak ditemukan pada database '%s'.\n", tableName,
               databaseName);
        return;
    }

    // Buka file tabel untuk dibaca
    FILE *file = fopen(filePath, "r");
    if (file == NULL) {
        printf("Gagal membuka tabel '%s'.\n", tableName);
        return;
    }

    // Baca header untuk mendapatkan indeks kolom yang sesuai dengan nama kolom
    char header[200];
    if (fgets(header, sizeof(header), file) == NULL) {
        printf("Gagal membaca header dari tabel '%s'.\n", tableName);
        fclose(file);
        return;
    }

    fclose(file);

    // Hapus karakter newline pada akhir header
    size_t headerLen = strlen(header);
    if (header[headerLen - 1] == '\n') {
        header[headerLen - 1] = '\0';
    }

    // Cari indeks kolom yang sesuai dengan nama kolom
    int columnIndex = -1;
    int index = 0, mark_idx_cols[cols_idx];
    char *token = strtok(header, ",");
    while (token != NULL) {
        // Hapus spasi di awal dan akhir token
        char columnNameTrimmed[50];
        sscanf(token, " %[^ ] ", columnNameTrimmed);
        // if (strcmp(columnNameTrimmed, columnName) == 0) {
        //   columnIndex = index;
        //   break;
        // }
        for (int i = 0; i < cols_idx; i++) {
            if (strcmp(columnNameTrimmed, cols_arr[i]) == 0) {
                columnIndex=0;
                mark_idx_cols[index] = 1;
            }
        }
        index++;
        token = strtok(NULL, ",");
    }

    // Jika kolom tidak ditemukan
    if (columnIndex == -1) {
        printf("Kolom '%s' tidak ditemukan dalam tabel '%s' pada database '%s'.\n",
               columnName, tableName, databaseName);
        // return;
    }

    // Buka file tabel kembali untuk membaca data dari kolom yang dipilih
    file = fopen(filePath, "r");
    if (file == NULL) {
        printf("Gagal membuka tabel '%s'.\n", tableName);
        return;
    }

    // Baca dan tampilkan data dari kolom yang dipilih
    char line[200];
    int lineNumber = 0;
    while (fgets(line, sizeof(line), file)) {
        lineNumber++;
        if (lineNumber == 1) {
            continue; // Skip the header line
        }
        char *token = strtok(line, ",");
        int columnCount = 0;
        char res[100];
        while (token != NULL) {
            // if (columnCount == columnIndex ||
            //     (columnIndex == index - 1 && token[strlen(token) - 1] == '\n')) {
            //   // Handle case for the last column
            //   printf("%s\n", token);
            //   break;
            // }
            if (mark_idx_cols[columnCount] == 1) {
                char tmp[100];
                sprintf(tmp, "%s,", token);
                strcat(res, tmp);
            }
            columnCount++;
            token = strtok(NULL, ",");
        }
        res[strlen(res)-1]= '\0';
        printf("%s\n", res);
        memset(res, 0, 100);
    }

    fclose(file);
}


int main() {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char command[100];

    // Membuat file deskriptor soket baru
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // Menset opsi soket untuk me-reuse alamat dan port
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(8080);

    // Binding soket ke alamat dan port yang ditentukan
    if (bind(server_fd, (struct sockaddr*)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    // Listening koneksi masuk
    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    while (1) {
        // Menerima koneksi baru
        if ((new_socket = accept(server_fd, (struct sockaddr*)&address, (socklen_t*)&addrlen)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        while (1) {
            // Menerima perintah dari client
            valread = read(new_socket, buffer, 1024);
            strncpy(command, buffer, valread);

            // Memproses perintah
            if (strncmp(command, "CREATE DATABASE", 15) == 0) {
                char databaseName[50];
                sscanf(command, "CREATE DATABASE %s;", databaseName);
                createDatabase(databaseName);
            } else if (strncmp(command, "CREATE TABLE", 12) == 0) {
                char databaseName[50];
                char tableName[50];
                char columns[200];
                sscanf(command, "CREATE TABLE %s (%[^;]s);", tableName, columns);
                char* token = strtok(tableName, ".");
                strcpy(databaseName, token);
                token = strtok(NULL, ".");
                strcpy(tableName, token);
                createTable(databaseName, tableName, columns);
            } else if (strncmp(command, "DROP DATABASE", 13) == 0) {
                char databaseName[50];
                sscanf(command, "DROP DATABASE %s;", databaseName);
                dropDatabase(databaseName);
            } else if (strncmp(command, "DROP TABLE", 10) == 0) {
                char databaseName[50];
                char tableName[50];
                sscanf(command, "DROP TABLE %s;", tableName);
                char* token = strtok(tableName, ".");
                strcpy(databaseName, token);
                token = strtok(NULL, ".");
                strcpy(tableName, token);
                dropTable(databaseName, tableName);
            } else if (strncmp(command, "DROP COLUMN", 11) == 0) {
                char databaseName[50];
                char tableName[50];
                char columnName[50];
                sscanf(command, "DROP COLUMN %s FROM %s;", columnName, tableName);
                char* token = strtok(tableName, ".");
                strcpy(databaseName, token);
                token = strtok(NULL, ".");
                strcpy(tableName, token);
                dropColumn(databaseName, tableName, columnName);
            } else if (strncmp(command, "INSERT INTO", 11) == 0) {
                char databaseName[50];
                char tableName[50];
                char values[200];
                sscanf(command, "INSERT INTO %[^.].%[^ ] %[^\n]", databaseName, tableName, values);
                insertRow(databaseName, tableName, values);
            } else if (strncmp(command, "UPDATE", 6) == 0) {
                char databaseName[50];
                char tableName[50];
                char columnName[50];
                char newValue[50];
                sscanf(command, "UPDATE %[^.].%[^ ] SET %[^=]=%s;", databaseName, tableName, columnName, newValue);
                updateColumnValue(databaseName, tableName, columnName, newValue);
            } else if (strncmp(command, "DELETE FROM", 11) == 0) {
                char databaseName[50];
                char tableName[50];
                sscanf(command, "DELETE FROM %[^.].%s;", databaseName, tableName);
                deleteRows(databaseName, tableName);
            } else if (strncmp(command, "SELECT * FROM", 13) == 0) {
                char databaseName[50];
                char tableName[50];
                sscanf(command, "SELECT * FROM %[^.].%s;", databaseName, tableName);
                selectAll(databaseName, tableName, new_socket);
            } else if (strncmp(command, "SELECT", 6) == 0) {
		      char databaseName[50];
		      char tableName[50];
		      char columnName[50];
		      sscanf(command, "SELECT %[^\n] FROM %[^.].%[^ ]", databaseName, tableName, columnName);
		      displayColumnData(databaseName, tableName, columnName);
            } else if (strncmp(command, "EXIT", 4) == 0) {
                break;
            } else {
                printf("Perintah tidak valid.\n");
            }

            // Mengirimkan status ke client
            char response[1024];
            sprintf(response, "Perintah berhasil diproses.\n");
            send(new_socket, response, strlen(response), 0);
        }

        close(new_socket);
    }

    return 0;
}
