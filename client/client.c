#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <time.h>

#define MAX_USERNAME_LENGTH 20
#define MAX_PASSWORD_LENGTH 20
#define MAX_LINE_LENGTH 100
#define MAX_LOG_LENGTH 100

int authenticate(const char *username, const char *password) {
    FILE *file = fopen("users.txt", "r");
    if (file == NULL) {
        printf("File users.txt tidak ditemukan.\n");
        return 0;
    }

    char line[MAX_LINE_LENGTH];
    while (fgets(line, sizeof(line), file) != NULL) {
        char storedUsername[MAX_USERNAME_LENGTH];
        char storedPassword[MAX_PASSWORD_LENGTH];

        // Mengurai baris menjadi username dan password yang tersimpan
        sscanf(line, "%s %s", storedUsername, storedPassword);

        // Memeriksa kesesuaian username dan password
        if (strcmp(username, storedUsername) == 0 && strcmp(password, storedPassword) == 0) {
            fclose(file);
            return 1; // Autentikasi berhasil
        }
    }

    fclose(file);
    return 0; // Autentikasi gagal
}

void logCommand(const char *username, const char *command) {
    time_t now;
    time(&now);
    char timestamp[MAX_LOG_LENGTH];
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", localtime(&now));

    FILE *file = fopen("log.txt", "a");
    if (file == NULL) {
        printf("File log.txt tidak dapat diakses.\n");
        return;
    }

    fprintf(file, "%s:%s:%s\n", timestamp, username, command);
    fclose(file);
}

int main(int argc, char *argv[]) {
    char username[MAX_USERNAME_LENGTH];
    char password[MAX_PASSWORD_LENGTH];

    // Cek jumlah argumen
    if (argc != 5) {
        printf("Format penggunaan: ./%s -u [username] -p [password]\n", argv[0]);
        return 1;
    }

    // Memeriksa opsi username dan password
    if (strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
        printf("Format penggunaan: ./%s -u [username] -p [password]\n", argv[0]);
        return 1;
    }

    // Mendapatkan nilai username dan password
    strncpy(username, argv[2], MAX_USERNAME_LENGTH);
    strncpy(password, argv[4], MAX_PASSWORD_LENGTH);

    // Memeriksa autentikasi dengan data pengguna yang ada di file users.txt
    if (!authenticate(username, password)) {
        printf("Log masuk gagal. Username atau password salah.\n");
        return 1;
    }

    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char command[100];

    // Membuat file deskriptor soket baru
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\nSocket creation error\n");
        return -1;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(8080);

    // Mengkonversi alamat IPv4 dan mengisinya ke struct sockaddr_in
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        printf("\nInvalid address/ Address not supported\n");
        return -1;
    }

    // Menghubungkan ke server
    if (connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed\n");
        return -1;
    }

    while (1) {
        printf("\nMasukkan perintah: ");
        fgets(command, sizeof(command), stdin);

        // Mengirim perintah ke server
        send(sock, command, strlen(command), 0);

        // Membaca respons dari server
        char buffer[1024] = {0};
        valread = read(sock, buffer, sizeof(buffer));
        if (valread > 0) {
            printf("Response from server:\n%s\n", buffer);
        }

        // Logging perintah
        logCommand(username, command);

        if (strncmp(command, "EXIT", 4) == 0) {
            break;
        }
    }

    return 0;
}
