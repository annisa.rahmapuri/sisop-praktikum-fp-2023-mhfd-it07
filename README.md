# Laporan Praktikum Sistem Operasi - Final Project

## Requirment Client.c

Pada program ini berhasil menjawab requirment soal di bawah ini :

- Terdapat *user* dengan ***username*** dan ***password***  yang digunakan untuk mengakses database yang merupakan haknya (*database* yang memiliki akses dengan *username* tersebut). Namun, jika *user* merupakan **root** (**sudo**), maka bisa mengakses semua *database* yang ada
    
    Format
    
    ./[program_client_database] -u [username] -p [password]
    
    ---
    
    Contoh
    
    ./client_databaseku -u khonsu -p khonsu123
    
    ---
    
- Setiap *command* yang dipakai harus dilakukan *logging* ke suatu *file* dengan format. Jika yang eksekusi **root**, maka *username* **root**.
    
    Format di dalam log
    
    timestamp(yyyy-mm-dd hh:mm:ss):username:command
    
    ---
    
    Contoh
    
    2021-05-19 02:05:15:khonsu:SELECT FROM table1
    
    ---
    
- Jika ada *command* yang tidak sesuai penggunaannya. Maka akan mengeluarkan pesan *error* **tanpa keluar dari program *client***.
- • Program *client* dan *dump* berkomunikasi **dengan socket** ke program utama dan **tidak boleh** langsung mengakses *file*-*file* di folder program utama.

### Penjelasan Client.c

Kami diminta untuk membuat program client.c dengan 

1. Header dan Konstanta
    
    ```c
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <unistd.h>
    #include <arpa/inet.h>
    #include <time.h>
    
    #define MAX_USERNAME_LENGTH 20
    #define MAX_PASSWORD_LENGTH 20
    #define MAX_LINE_LENGTH 100
    #define MAX_LOG_LENGTH 100
    ```
    
    Bagian ini berisi pemanggilan header-file yang diperlukan untuk fungsi-fungsi standar serta definisi beberapa konstanta yang digunakan dalam program.
    
2. Fungsi Authentikasi
    
    ```c
    int authenticate(const char *username, const char *password) {
        // Membuka file users.txt
        FILE *file = fopen("users.txt", "r");
        if (file == NULL) {
            printf("File users.txt tidak ditemukan.\n");
            return 0;
        }
    
        char line[MAX_LINE_LENGTH];
        while (fgets(line, sizeof(line), file) != NULL) {
            char storedUsername[MAX_USERNAME_LENGTH];
            char storedPassword[MAX_PASSWORD_LENGTH];
    
            // Mengurai baris menjadi username dan password yang tersimpan
            sscanf(line, "%s %s", storedUsername, storedPassword);
    
            // Memeriksa kesesuaian username dan password
            if (strcmp(username, storedUsername) == 0 && strcmp(password, storedPassword) == 0) {
                fclose(file);
                return 1; // Autentikasi berhasil
            }
        }
    
        fclose(file);
        return 0; // Autentikasi gagal
    }
    ```
    
    Fungsi **`authenticate`** digunakan untuk memeriksa autentikasi pengguna. Disini saya membuat fungsinya dengan cara membandingkan username dan password yang diberikan dengan data pengguna yang tersimpan dalam file "users.txt". 
    
3. Fungsi Logging
    
    ```c
    void logCommand(const char *username, const char *command) {
        // Mendapatkan waktu saat ini
        time_t now;
        time(&now);
        char timestamp[MAX_LOG_LENGTH];
        strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", localtime(&now));
    
        // Membuka file log.txt
        FILE *file = fopen("log.txt", "a");
        if (file == NULL) {
            printf("File log.txt tidak dapat diakses.\n");
            return;
        }
    
        // Menulis log perintah ke file
        fprintf(file, "%s:%s:%s\n", timestamp, username, command);
        fclose(file);
    }
    ```
    
    Fungsi **`logCommand`** digunakan untuk mencatat (logging) perintah yang dikirimkan oleh pengguna ke file "log.txt". Fungsi ini mendapatkan waktu saat ini menggunakan **`time`**, memformatnya menjadi string menggunakan **`strftime`**, dan menulis log command yang diinput ke file menggunakan **`fprintf`** 
    
4. Format Input 
    
    ```c
    int main(int argc, char *argv[]) {
        // Deklarasi variabel username dan password
        char username[MAX_USERNAME_LENGTH];
        char password[MAX_PASSWORD_LENGTH];
    
        // Memeriksa jumlah argumen
        if (argc != 5) {
            printf("Format penggunaan: ./%s -u [username] -p [password]\n", argv[0]);
            return 1;
        }
    
        // Memeriksa opsi username dan password
        if (strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
            printf("Format penggunaan: ./%s -u [username] -p [password]\n", argv[0]);
            return 1;
        }
    
        // Mengisi variabel username dan password dari argumen
        strncpy(username, argv[2], MAX_USERNAME_LENGTH);
        strncpy(password, argv[4], MAX_PASSWORD_LENGTH);
    
        // Memeriksa autentikasi dengan data pengguna yang ada di file users.txt
        if (!authenticate(username, password)) {
            printf("Log masuk gagal. Username atau password salah.\n");
            return 1;
        }
    
        // ...
    }
    ```
    
    Pada bagian ini, program memeriksa jumlah argumen yang diberikan saat menjalankan program dan memeriksa opsi yang digunakan untuk menyediakan username dan password. 
    
    - Jika argumen tidak sesuai, program akan menampilkan pesan kesalahan dan keluar dengan status 1.
    - Jika autentikasi pengguna tidak berhasil, program akan menampilkan pesan kesalahan dan keluar dengan status 1.
    - Jika autentikasi berhasil, program akan melanjutkan eksekusi.
5. Socket 
    
    ```c
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char command[100];
    
    // Membuat file deskriptor soket baru
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\nSocket creation error\n");
        return -1;
    }
    
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(8080);
    
    // Mengkonversi alamat IPv4 dan mengisinya ke struct sockaddr_in
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        printf("\nInvalid address/ Address not supported\n");
        return -1;
    }
    
    // Menghubungkan ke server
    if (connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed\n");
        return -1;
    }
    
    while (1) {
        printf("\nMasukkan perintah: ");
        fgets(command, sizeof(command), stdin);
    
        // Mengirim perintah ke server
        send(sock, command, strlen(command), 0);
    
        // Membaca respons dari server
        char buffer[1024] = {0};
        valread = read(sock, buffer, sizeof(buffer));
        if (valread > 0) {
            printf("Response from server:\n%s\n", buffer);
        }
    
        // Logging perintah
        logCommand(username, command);
    
        if (strncmp(command, "EXIT", 4) == 0) {
            break;
        }
    }
    
    return 0;
    ```
    
    Pada bagian ini, program membuat soket baru menggunakan **`socket`** dengan domain alamat IPv4 dan tipe soket TCP. Kemudian, program mengisi struktur **`serv_addr`** dengan alamat IP dan port tujuan yang ditentukan. Selanjutnya, program menghubungkan soket dengan server menggunakan **`connect`**.
    
    Dalam loop **`while`**, program meminta pengguna untuk memasukkan perintah dari stdin (keyboard) menggunakan **`fgets`**. Perintah tersebut kemudian dikirimkan ke server menggunakan **`send`**. Program juga membaca respons dari server menggunakan **`read`** dan mencetaknya ke layar. Setelah itu, program memanggil fungsi **`logCommand`** untuk mencatat perintah yang dikirimkan. Jika perintah adalah "EXIT", program keluar dari loop dan menutup koneksi soket sebelum berakhir.

## Output client.c 

Socket
<a href="https://ibb.co/hMM8bHV"><img src="https://i.ibb.co/H22g6Yq/Screenshot-2023-06-24-215754.png" alt="Screenshot-2023-06-24-215754" border="0"></a>

Log.txt
<img src="https://i.ibb.co/B3fhTZG/Screenshot-2023-06-24-202956.png" alt="Screenshot-2023-06-24-202956" border="0">

Error Handling 
<a href="https://ibb.co/Wkprh1h"><img src="https://i.ibb.co/7nN3fFf/Screenshot-2023-06-24-215207.png" alt="Screenshot-2023-06-24-215207" border="0"></a>

## Requirment database.c

Pada program ini berhasil menjawab semua requirment

### Penjelasan Client.c

Kami diminta untuk membuat program client.c dengan 

1. Header dan Konstanta
    
    ```c
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <unistd.h>
    #include <arpa/inet.h>
    #include <stdbool.h>
    #include <sys/stat.h>
    ```
    
    Bagian ini berisi pemanggilan header-file yang diperlukan. 
    
2. Fungsi DDL
    1. Create Database 
        
        ```c
        void createDatabase(char *name) {
            // Periksa validitas nama database (hanya angka dan huruf)
            for (int i = 0; i < strlen(name); i++) {
                if (!((name[i] >= 'a' && name[i] <= 'z') || (name[i] >= 'A' && name[i] <= 'Z') || (name[i] >= '0' && name[i] <= '9'))) {
                    printf("Nama database tidak valid. Gunakan hanya huruf dan angka.\n");
                    return;
                }
            }
        
            // Buat direktori dengan nama database
            char dirname[256];
            snprintf(dirname, sizeof(dirname), "databaseku/%s", name);
            int status = mkdir(dirname, 0777);
        
            if (status == 0) {
                printf("Database '%s' berhasil dibuat.\n", name);
            } else {
                printf("Gagal membuat database '%s'.\n", name);
            }
        }
        ```
        
        Fungsi ini berguna untuk membuat database baru dengan menerima argumen berupa string **`name`**, yang merupakan nama database yang ingin dibuat.
        
        - Pertama, fungsi akan memeriksa validitas nama database dengan memeriksa apakah hanya terdiri dari huruf dan angka.
        - Jika valid, fungsi akan membuat direktori baru dengan menggunakan nama database sebagai nama direktori. Direktori ini akan dibuat di dalam folder "databaseku".
        - Fungsi akan mencetak pesan berhasil atau gagal sesuai dengan status pembuatan database.
    2. Create Table
        
        ```c
        void createTable(char *databaseName, char *tableName, char *columns) {
            // Periksa validitas nama tabel (hanya angka dan huruf)
            for (int i = 0; i < strlen(tableName); i++) {
                if (!((tableName[i] >= 'a' && tableName[i] <= 'z') || (tableName[i] >= 'A' && tableName[i] <= 'Z') || (tableName[i] >= '0' && tableName[i] <= '9'))) {
                    printf("Nama tabel tidak valid. Gunakan hanya huruf dan angka.\n");
                    return;
                }
            }
        
            // Buat path file tabel dalam database
            char filePath[256];
            snprintf(filePath, sizeof(filePath), "databaseku/%s/%s.csv", databaseName, tableName);
        
            // Periksa apakah database sudah ada
            char databasePath[256];
            snprintf(databasePath, sizeof(databasePath), "databaseku/%s", databaseName);
            struct stat st;
            if (stat(databasePath, &st) != 0 || !S_ISDIR(st.st_mode)) {
                printf("Database '%s' tidak ditemukan.\n", databaseName);
                return;
            }
        
            // Buka file untuk ditulis
            FILE *file = fopen(filePath, "w");
            if (file == NULL) {
                printf("Gagal membuat tabel '%s'.\n", tableName);
                return;
            }
            
            // Hapus tanda ")" pada judul kolom
            int len = strlen(columns);
            columns[len - 2] = '\0';
            
            // Tulis header file csv dengan nama kolom saja
            fprintf(file, "%s\n", columns);
            
            fclose(file);
        
            printf("Tabel '%s' berhasil dibuat pada database '%s'.\n", tableName, databaseName);
        }
        ```
        
        Fungsi ini digunakan untuk membuat tabel baru di dalam sebuah database dengan menerima tiga argumen: **`databaseName`** (nama database), **`tableName`** (nama tabel yang ingin dibuat), dan **`columns`** (judul kolom tabel).
        
        - Pertama, memeriksa validitas nama tabel dengan memeriksa apakah hanya terdiri dari huruf dan angka.
        - Selanjutnya, fungsi akan memeriksa apakah database yang ditentukan ada dengan memeriksa keberadaan direktori yang sesuai dengan nama database.
        - Jika database ditemukan, fungsi akan membuat file tabel dalam format CSV dengan menggunakan **`databaseName`** dan **`tableName`** untuk membentuk path file.
        - Fungsi akan menulis judul kolom ke file tabel yang telah dibuat.
        - Fungsi akan mencetak pesan berhasil atau gagal sesuai dengan status pembuatan tabel.
    3. Drop Database 
        
        ```c
        void dropDatabase(char *name) {
            // Buat path folder database
            char dirname[256];
            snprintf(dirname, sizeof(dirname), "databaseku/%s", name);
        
            // Hapus folder database
            int status = rmdir(dirname);
        
            if (status == 0) {
                printf("Database '%s' berhasil dihapus.\n", name);
            } else {
                printf("Gagal menghapus database '%s'.\n", name);
            }
        }
        ```
        
        Fungsi ini digunakan untuk menghapus sebuah database beserta isinya dengan menerima argumen berupa string **`name`**, yang merupakan nama database yang ingin dihapus.
        
        - Fungsi akan menghapus direktori database yang sesuai dengan nama yang diberikan.
        - Fungsi akan mencetak pesan berhasil atau gagal sesuai dengan status penghapusan database.
    4. Drop Table 
        
        ```c
        void dropTable(char *databaseName, char *tableName) {
            // Buat path file tabel dalam database
            char filePath[256];
            snprintf(filePath, sizeof(filePath), "databaseku/%s/%s.csv", databaseName, tableName);
        
            // Periksa apakah tabel sudah ada
            if (access(filePath, F_OK) == -1) {
                printf("Tabel '%s' tidak ditemukan pada database '%s'.\n", tableName, databaseName);
                return;
            }
        
            // Hapus file tabel
            int status = remove(filePath);
        
            if (status == 0) {
                printf("Tabel '%s' berhasil dihapus dari database '%s'.\n", tableName, databaseName);
            } else {
                printf("Gagal menghapus tabel '%s' dari database '%s'.\n", tableName, databaseName);
            }
        }
        ```
        
        Fungsi ini bertugas menghapus sebuah tabel dari database dengan menerima dua argumen: **`databaseName`** (nama database tempat tabel berada) dan **`tableName`** (nama tabel yang akan dihapus).
        
        - Fungsi akan memeriksa keberadaan tabel dengan memeriksa apakah file tabel dengan path yang sesuai ada.
        - Jika tabel ditemukan, fungsi akan menghapus file tabel tersebut.
        - Fungsi akan mencetak pesan berhasil atau gagal sesuai dengan status penghapusan tabel.
    5. Drop Column 
        
        ```c
        void dropColumn(char *databaseName, char *tableName, char *columnName) {
            // Buat path file tabel dalam database
            char filePath[256];
            snprintf(filePath, sizeof(filePath), "databaseku/%s/%s.csv", databaseName, tableName);
        
            // Periksa apakah tabel sudah ada
            if (access(filePath, F_OK) == -1) {
                printf("Tabel '%s' tidak ditemukan pada database '%s'.\n", tableName, databaseName);
                return;
            }
        
            // Buka file tabel untuk dibaca
            FILE *file = fopen(filePath, "r");
            if (file == NULL) {
                printf("Gagal membuka tabel '%s'.\n", tableName);
                return;
            }
        
            // Baca baris pertama (header)
            char line[200];
            fgets(line, sizeof(line), file);
        
            // Cari indeks kolom yang akan dihapus
            int columnIndex = -1;
            char *token = strtok(line, ",");
            int index = 0;
            while (token != NULL) {
                if (strcmp(token, columnName) == 0) {
                    columnIndex = index;
                    break;
                }
                index++;
                token = strtok(NULL, ",");
            }
        
            fclose(file);
        
            // Jika kolom tidak ditemukan
            if (columnIndex == -1) {
                printf("Kolom '%s' tidak ditemukan dalam tabel '%s' pada database '%s'.\n", columnName, tableName, databaseName);
                return;
            }
        
            // Buat file temporary untuk menulis hasil penghapusan kolom
            char tempFilePath[256];
            snprintf(tempFilePath, sizeof(tempFilePath), "databaseku/%s/temp.csv", databaseName);
            FILE *tempFile = fopen(tempFilePath, "w");
            if (tempFile == NULL) {
                printf("Gagal membuat file temporary.\n");
                return;
            }
        
            // Buka file tabel asli untuk dibaca
            file = fopen(filePath, "r");
            if (file == NULL) {
                printf("Gagal membuka tabel '%s'.\n", tableName);
                fclose(tempFile);
                remove(tempFilePath);
                return;
            }
        
            // Baca baris-baris selanjutnya dan tulis ke file temporary tanpa kolom yang dihapus
            while (fgets(line, sizeof(line), file)) {
                char *token = strtok(line, ",");
                int columnCount = 0;
                while (token != NULL) {
                    columnCount++;
                    if (columnCount != columnIndex + 1) {
                        fprintf(tempFile, "%s,", token);
                    }
                    token = strtok(NULL, ",");
                }
                fprintf(tempFile, "\n");
            }
        
            fclose(file);
            fclose(tempFile);
        
            // Hapus file tabel asli
            remove(filePath);
        
            // Rename file temporary menjadi file tabel asli
            rename(tempFilePath, filePath);
        
            printf("Kolom '%s' berhasil dihapus dari tabel '%s' pada database '%s'.\n", columnName, tableName, databaseName);
        }
        ```
        
        Fungsi ini digunakan untuk menghapus kolom tertentu dari sebuah tabel dalam database dengan menerima tiga argumen: **`databaseName`** (nama database tempat tabel berada), **`tableName`** (nama tabel), dan **`columnName`** (nama kolom yang akan dihapus).
        
        - Fungsi akan memeriksa keberadaan tabel dengan memeriksa apakah file tabel dengan path yang sesuai ada.
        - Jika tabel ditemukan, fungsi akan membuka file tabel untuk dibaca dan mencari indeks kolom yang sesuai dengan **`columnName`**.
        - Jika kolom ditemukan, fungsi akan membuat file temporary untuk menulis hasil penghapusan kolom.
        - Fungsi akan membaca baris-baris tabel asli, menyalin nilai-nilai ke file temporary tanpa kolom yang dihapus, dan menghapus file tabel asli.
        - Selanjutnya, file temporary akan direname menjadi file tabel asli.
        - Fungsi akan mencetak pesan berhasil atau gagal sesuai dengan status penghapusan kolom.
3. Fungsi DML
    1. Insert 
        
        ```c
        void insertRow(char *databaseName, char *tableName, char *values) {
            // Buat path file tabel dalam database
            char filePath[256];
            snprintf(filePath, sizeof(filePath), "databaseku/%s/%s.csv", databaseName, tableName);
        
            // Periksa apakah tabel sudah ada
            if (access(filePath, F_OK) == -1) {
                printf("Tabel '%s' tidak ditemukan pada database '%s'.\n", tableName, databaseName);
                return;
            }
        
            // Buka file tabel untuk ditulis (mode "a" agar data ditambahkan di akhir file)
            FILE *file = fopen(filePath, "a");
            if (file == NULL) {
                printf("Gagal membuka tabel '%s'.\n", tableName);
                return;
            }
        
            // Pisahkan nilai-nilai pada perintah INSERT
            char *token = strtok(values, ",");
            char line[200] = "";
            bool firstValue = true;
        
            // Tulis nilai-nilai ke file tabel
            while (token != NULL) {
                // Hapus karakter spasi di awal dan akhir nilai
                while (*token == ' ' || *token == '(' || *token == '\'')
                    token++;
                size_t len = strlen(token);
                while (len > 0 && (token[len - 1] == ' ' || token[len - 1] == ')' || token[len - 1] == '\''))
                    token[--len] = '\0';
        
                // Tulis nilai ke baris
                if (firstValue) {
                    firstValue = false;
                    snprintf(line + strlen(line), sizeof(line) - strlen(line), "%s", token);
                } else {
                    snprintf(line + strlen(line), sizeof(line) - strlen(line), ",%s", token);
                }
        
                token = strtok(NULL, ",");
            }
        
            fprintf(file, "%s\n", line);
            fclose(file);
        
            printf("Data berhasil ditambahkan pada tabel '%s' di database '%s'.\n", tableName, databaseName);
        }
        ```
        
        Fungsi ini digunakan untuk memasukkan baris baru ke dalam tabel dengan menerima tiga argumen: **`databaseName`** (nama database tempat tabel berada), **`tableName`** (nama tabel), dan **`values`** (nilai-nilai untuk baris baru yang akan dimasukkan).
        
        - Pertama, fungsi membangun path file tabel dengan menggunakan **`snprintf`** dan **`databaseName`** serta **`tableName`**.
        - Fungsi melakukan pemeriksaan apakah tabel tersebut sudah ada atau belum dengan menggunakan **`access`** dan **`F_OK`**. Jika tabel tidak ditemukan, maka fungsi mencetak pesan kesalahan.
        - Selanjutnya, fungsi membuka file tabel dalam mode "a" (append) dengan **`fopen`** untuk menambahkan data baru di akhir file. Jika file tidak dapat dibuka, fungsi mencetak pesan kesalahan.
        - Fungsi melakukan pemisahan nilai-nilai pada **`values`** menggunakan **`strtok`** dengan delimiter koma (**`,`**) untuk mendapatkan nilai-nilai yang akan dimasukkan ke dalam baris baru.
        - Setiap nilai diproses dengan menghapus karakter-karakter tidak diperlukan seperti spasi, tanda kurung buka dan tutup, serta tanda kutip tunggal. Nilai-nilai tersebut ditambahkan ke dalam variabel **`line`** yang akan merepresentasikan baris baru.
        - Setelah selesai memproses semua nilai, fungsi menggunakan **`fprintf`** untuk menulis **`line`** ke dalam file tabel dan kemudian menutup file dengan **`fclose`**.
        - Terakhir, fungsi mencetak pesan berhasil bahwa data telah ditambahkan ke dalam tabel.
    2. Update 
        
        ```c
        void updateColumnValue(char *databaseName, char *tableName, char *columnName, char *newValue) {
            // Buat path file tabel dalam database
            char filePath[256];
            snprintf(filePath, sizeof(filePath), "databaseku/%s/%s.csv", databaseName, tableName);
        
            // Periksa apakah tabel sudah ada
            if (access(filePath, F_OK) == -1) {
                printf("Tabel '%s' tidak ditemukan pada database '%s'.\n", tableName, databaseName);
                return;
            }
        
            // Buka file tabel untuk dibaca
            FILE *file = fopen(filePath, "r");
            if (file == NULL) {
                printf("Gagal membuka tabel '%s'.\n", tableName);
                return;
            }
        
            // Baca baris pertama (header)
            char line[200];
            fgets(line, sizeof(line), file);
        
            // Cari indeks kolom yang akan diupdate
            int columnIndex = -1;
            char *token = strtok(line, ",");
            int index = 0;
            while (token != NULL) {
                if (strcmp(token, columnName) == 0) {
                    columnIndex = index;
                    break;
                }
                index++;
                token = strtok(NULL, ",");
            }
        
            fclose(file);
        
            // Jika kolom tidak ditemukan
            if (columnIndex == -1) {
                printf("Kolom '%s' tidak ditemukan dalam tabel '%s' pada database '%s'.\n", columnName, tableName, databaseName);
                return;
            }
        
            // Buat file temporary untuk menulis hasil update kolom
            char tempFilePath[256];
            snprintf(tempFilePath, sizeof(tempFilePath), "databaseku/%s/temp.csv", databaseName);
            FILE *tempFile = fopen(tempFilePath, "w");
            if (tempFile == NULL) {
                printf("Gagal membuat file temporary.\n");
                return;
            }
        
            // Buka file tabel asli untuk dibaca
            file = fopen(filePath, "r");
            if (file == NULL) {
                printf("Gagal membuka tabel '%s'.\n", tableName);
                fclose(tempFile);
                remove(tempFilePath);
                return;
            }
        
            // Baca baris pertama (header) dan tulis ke file temporary
            fgets(line, sizeof(line), file);
            fprintf(tempFile, "%s", line);
        
            // Baca baris-baris selanjutnya dan tulis ke file temporary dengan kolom yang diupdate
            while (fgets(line, sizeof(line), file)) {
                char *token = strtok(line, ",");
                int columnCount = 0;
                while (token != NULL) {
                    columnCount++;
                    if (columnCount == columnIndex + 1) {
                        fprintf(tempFile, "%s,", newValue);
                    } else {
                        fprintf(tempFile, "%s,", token);
                    }
                    token = strtok(NULL, ",");
                }
                fprintf(tempFile, "\n");
            }
        
            fclose(file);
            fclose(tempFile);
        
            // Hapus file tabel asli
            remove(filePath);
        
            // Ganti nama file temporary menjadi file tabel asli
            rename(tempFilePath, filePath);
        
            printf("Kolom '%s' pada tabel '%s' dalam database '%s' berhasil diupdate.\n", columnName, tableName, databaseName);
        }
        ```
        
        Fungsi ini digunakan untuk mengupdate nilai-nilai dalam suatu kolom di dalam tabel dengan menerima empat argumen: **`databaseName`** (nama database tempat tabel berada), **`tableName`** (nama tabel), **`columnName`** (nama kolom yang akan diupdate), dan **`newValue`** (nilai baru yang akan diberikan pada kolom tersebut).
        
        - Pertama, fungsi akan memeriksa keberadaan tabel dengan memeriksa apakah file tabel dengan path yang sesuai ada.
        - Jika tabel ditemukan, fungsi membaca baris pertama (header) untuk mencari indeks kolom yang sesuai dengan **`columnName`**.
        - Setelah itu, fungsi membuka file tabel untuk dibaca dan membuat file temporary untuk menulis hasil update kolom.
        - Fungsi membaca baris-baris tabel asli, menyalin nilai-nilai ke file temporary dengan kolom yang diupdate, dan menghapus file tabel asli.
        - Terakhir, file temporary diubah namanya menjadi file tabel asli.
        - Fungsi mencetak pesan berhasil atau gagal sesuai dengan status update kolom.
    3. Delete
        
        ```c
        void deleteRows(char *databaseName, char *tableName) {
            // Create the file path for the table within the database
            char filePath[256];
            snprintf(filePath, sizeof(filePath), "databaseku/%s/%s.csv", databaseName, tableName);
        
            // Check if the table already exists
            if (access(filePath, F_OK) == -1) {
                printf("Table '%s' not found in database '%s'.\n", tableName, databaseName);
                return;
            }
        
            // Open the table file for reading
            FILE *file = fopen(filePath, "r");
            if (file == NULL) {
                printf("Failed to open table '%s'.\n", tableName);
                return;
            }
        
            // Read the first line (header)
            char line[200];
            fgets(line, sizeof(line), file);
            
            fclose(file);
        
            // Open the table file for writing (this will delete all the rows)
            file = fopen(filePath, "w");
            if (file == NULL) {
                printf("Failed to open table '%s'.\n", tableName);
                return;
            }
        
            // Write the header back into the file
            fprintf(file, "%s", line);
            
            fclose(file);
        
            printf("All rows have been deleted from table '%s' in database '%s'.\n", tableName, databaseName);
        }
        ```
        
        Fungsi ini digunakan untuk menghapus semua baris dalam sebuah tabel dengan menerima dua argumen: **`databaseName`** (nama database tempat tabel berada) dan **`tableName`** (nama tabel yang akan dihapus barisnya).
        
        - Pertama, fungsi ini akan memeriksa keberadaan tabel dengan memeriksa apakah file tabel dengan path yang sesuai ada.
        - Jika tabel ditemukan, fungsi membuka file tabel dalam mode "r" (read) dan membaca baris pertama (header).
        - Setelah itu, fungsi membuka file tabel kembali dalam mode "w" (write) untuk menghapus semua baris.
        - Fungsi menulis kembali header ke file tabel yang telah dikosongkan.
        - Fungsi mencetak pesan berhasil setelah berhasil menghapus semua baris dari tabel.
    4. Select All 
        
        ```c
        void selectAll(char *databaseName, char *tableName, int new_socket) {
            // Buat path file tabel dalam database
            char filePath[256];
            snprintf(filePath, sizeof(filePath), "databaseku/%s/%s.csv", databaseName, tableName);
        
            // Periksa apakah tabel sudah ada
            if (access(filePath, F_OK) == -1) {
                printf("Tabel '%s' tidak ditemukan pada database '%s'.\n", tableName, databaseName);
                return;
            }
        
            // Buka file tabel untuk dibaca
            FILE *file = fopen(filePath, "r");
            if (file == NULL) {
                printf("Gagal membuka tabel '%s'.\n", tableName);
                return;
            }
        
            // Baca dan tampilkan seluruh isi file
            char line[200];
            while (fgets(line, sizeof(line), file)) {
                printf("%s", line);
                send(new_socket, line, strlen(line), 0);
            }
           
        
            fclose(file);
        }
        ```
        
        Fungsi ini menerima tiga argumen: **`databaseName`** (nama database), **`tableName`** (nama tabel), dan **`new_socket`** (socket baru untuk mengirimkan data).
        
        - Pertama, fungsi membentuk path file tabel dengan menggunakan **`snprintf`** berdasarkan **`databaseName`** dan **`tableName`**.
        - Kemudian, fungsi memeriksa keberadaan tabel dengan menggunakan **`access`** untuk memeriksa apakah file tabel ada.
        - Jika tabel tidak ditemukan, fungsi mencetak pesan error dan mengembalikan fungsi.
        - Selanjutnya, fungsi membuka file tabel dalam mode "r" (read).
        - Fungsi menggunakan loop **`while`** dan **`fgets`** untuk membaca setiap baris dari file tabel dan menampilkannya dengan **`printf`**.
        - Setelah mencetak baris, fungsi juga mengirimkan baris tersebut melalui **`send`** ke **`new_socket`**.
        - Terakhir, setelah selesai membaca file tabel, fungsi menutup file dengan **`fclose`**.
    5. Select Column
        
        ```c
        void displayColumnData(char *databaseName, char *tableName, char *columnName) {
            // Buat path file tabel dalam database
            char filePath[256], cols_arr[10][100];
            char *tmp_token;
            tmp_token = strtok(columnName, ", ");
            int cols_idx = 0;
            while (tmp_token != NULL) {
                strcpy(cols_arr[cols_idx], tmp_token);
                cols_idx++;
                tmp_token = strtok(NULL, ", ");
            }
            snprintf(filePath, sizeof(filePath), "databaseku/%s/%s.csv", databaseName,
                     tableName);
        
            // Periksa apakah tabel sudah ada
            if (access(filePath, F_OK) == -1) {
                printf("Tabel '%s' tidak ditemukan pada database '%s'.\n", tableName,
                       databaseName);
                return;
            }
        
            // Buka file tabel untuk dibaca
            FILE *file = fopen(filePath, "r");
            if (file == NULL) {
                printf("Gagal membuka tabel '%s'.\n", tableName);
                return;
            }
        
            // Baca header untuk mendapatkan indeks kolom yang sesuai dengan nama kolom
            char header[200];
            if (fgets(header, sizeof(header), file) == NULL) {
                printf("Gagal membaca header dari tabel '%s'.\n", tableName);
                fclose(file);
                return;
            }
        
            fclose(file);
        
            // Hapus karakter newline pada akhir header
            size_t headerLen = strlen(header);
            if (header[headerLen - 1] == '\n') {
                header[headerLen - 1] = '\0';
            }
        
            // Cari indeks kolom yang sesuai dengan nama kolom
            int columnIndex = -1;
            int index = 0, mark_idx_cols[cols_idx];
            char *token = strtok(header, ",");
            while (token != NULL) {
                // Hapus spasi di awal dan akhir token
                char columnNameTrimmed[50];
                sscanf(token, " %[^ ] ", columnNameTrimmed);
                // if (strcmp(columnNameTrimmed, columnName) == 0) {
                //   columnIndex = index;
                //   break;
                // }
                for (int i = 0; i < cols_idx; i++) {
                    if (strcmp(columnNameTrimmed, cols_arr[i]) == 0) {
                        columnIndex=0;
                        mark_idx_cols[index] = 1;
                    }
                }
                index++;
                token = strtok(NULL, ",");
            }
        
            // Jika kolom tidak ditemukan
            if (columnIndex == -1) {
                printf("Kolom '%s' tidak ditemukan dalam tabel '%s' pada database '%s'.\n",
                       columnName, tableName, databaseName);
                // return;
            }
        
            // Buka file tabel kembali untuk membaca data dari kolom yang dipilih
            file = fopen(filePath, "r");
            if (file == NULL) {
                printf("Gagal membuka tabel '%s'.\n", tableName);
                return;
            }
        
            // Baca dan tampilkan data dari kolom yang dipilih
            char line[200];
            int lineNumber = 0;
            while (fgets(line, sizeof(line), file)) {
                lineNumber++;
                if (lineNumber == 1) {
                    continue; // Skip the header line
                }
                char *token = strtok(line, ",");
                int columnCount = 0;
                char res[100];
                while (token != NULL) {
                    // if (columnCount == columnIndex ||
                    //     (columnIndex == index - 1 && token[strlen(token) - 1] == '\n')) {
                    //   // Handle case for the last column
                    //   printf("%s\n", token);
                    //   break;
                    // }
                    if (mark_idx_cols[columnCount] == 1) {
                        char tmp[100];
                        sprintf(tmp, "%s,", token);
                        strcat(res, tmp);
                    }
                    columnCount++;
                    token = strtok(NULL, ",");
                }
                res[strlen(res)-1]= '\0';
                printf("%s\n", res);
                memset(res, 0, 100);
            }
        
            fclose(file);
        }
        ```
        
        Fungsi ini menerima tiga argumen: **`databaseName`** (nama database), **`tableName`** (nama tabel), dan **`columnName`** (nama kolom).
        
        - Pertama, fungsi membentuk path file tabel dengan menggunakan **`snprintf`** berdasarkan **`databaseName`** dan **`tableName`**.
        - Kemudian, fungsi memeriksa keberadaan tabel dengan menggunakan **`access`** untuk memeriksa apakah file tabel ada.
        - Jika tabel tidak ditemukan, fungsi mencetak pesan error dan mengembalikan fungsi.
        - Selanjutnya, fungsi membuka file tabel dalam mode "r" (read).
        - Fungsi membaca header dari file tabel menggunakan **`fgets`** dan menyimpannya dalam variabel **`header`**.
        - Setelah membaca header, fungsi menutup file tabel dengan **`fclose`**.
        - Fungsi menghapus karakter newline (**`\n`**) pada akhir header jika ada.
        - Selanjutnya, fungsi mencari indeks kolom yang sesuai dengan **`columnName`** dengan menggunakan loop **`while`** dan **`strtok`**.
        - Setiap token yang ditemukan di header akan dibandingkan dengan **`columnName`** menggunakan **`strcmp`**.
        - Jika ditemukan kolom yang sesuai, fungsi akan menandai kolom tersebut dengan memasukkan nilai 1 ke dalam array **`mark_idx_cols`** pada indeks yang sama dengan indeks kolom.
        - Setelah selesai mencari indeks kolom, fungsi membuka file tabel kembali dalam mode "r" (read).
        - Fungsi menggunakan loop **`while`** dan **`fgets`** untuk membaca setiap baris dari file tabel, kecuali baris header.
        - Dalam loop, setiap baris dibagi menjadi token menggunakan **`strtok`**.
        - Setiap token yang sesuai dengan kolom yang ditandai akan digabungkan menjadi satu string menggunakan **`strcat`** dan disimpan dalam variabel **`res`**.
        - Setelah selesai membaca token, fungsi mencetak string **`res`** sebagai hasil data kolom yang dipilih dengan **`printf`**.
        - Terakhir, setelah selesai membaca file tabel, fungsi menutup file dengan **`fclose`**.
4. Insialiasai Variable dan Pembuatan Socket 
    
    ```c
    int main() {
        int server_fd, new_socket, valread;
        struct sockaddr_in address;
        int opt = 1;
        int addrlen = sizeof(address);
        char buffer[1024] = {0};
        char command[100];
    
        // Membuat file deskriptor soket baru
        if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
            perror("socket failed");
            exit(EXIT_FAILURE);
        }
    
        // Menset opsi soket untuk me-reuse alamat dan port
        if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
            perror("setsockopt");
            exit(EXIT_FAILURE);
        }
        address.sin_family = AF_INET;
        address.sin_addr.s_addr = INADDR_ANY;
        address.sin_port = htons(8080);
    
        // Binding soket ke alamat dan port yang ditentukan
        if (bind(server_fd, (struct sockaddr*)&address, sizeof(address)) < 0) {
            perror("bind failed");
            exit(EXIT_FAILURE);
        }
    
        // Listening koneksi masuk
        if (listen(server_fd, 3) < 0) {
            perror("listen");
            exit(EXIT_FAILURE);
        }
    ```
    
    - Variabel-variabel yang diperlukan seperti **`server_fd`**, **`new_socket`**, **`valread`**, **`address`**, **`opt`**, **`addrlen`**, **`buffer`**, dan **`command`** dideklarasikan di awal program. Variabel-variabel ini akan digunakan dalam proses pembuatan soket, pengiriman dan penerimaan data, serta pemrosesan perintah dari klien.
    - Pada bagian ini, sebuah soket baru dibuat menggunakan fungsi **`socket()`**. Soket ini akan digunakan untuk menerima koneksi dari klien.
    - Fungsi **`setsockopt()`** digunakan untuk mengatur opsi soket agar dapat menggunakan kembali alamat dan port yang sama setelah ditutup sebelumnya.
    - Struktur **`address`** diinisialisasi dengan alamat dan port yang ditentukan.
    - Soket diikat (**`bind()`**) ke alamat dan port yang ditentukan untuk mendengarkan koneksi masuk.
5. Menjalankan Server
    
    ```c
    while (1) {
            // Menerima koneksi baru
            if ((new_socket = accept(server_fd, (struct sockaddr*)&address, (socklen_t*)&addrlen)) < 0) {
                perror("accept");
                exit(EXIT_FAILURE);
            }
    ```
    
    - Server akan terus berjalan dalam loop utama **`while (1)`** untuk menerima koneksi dan memproses perintah dari klien.
    - Setelah soket diikat, server akan mulai mendengarkan koneksi masuk menggunakan fungsi **`listen()`**.
    - Saat ada koneksi baru yang diterima (**`accept()`**), soket baru akan dibuat untuk koneksi tersebut dan perintah dari klien akan diproses dalam loop **`while (1)`**.
6. Menerima dan Memproses Perintah dari Client.c
    
    ```c
    while (1) {
                // Menerima perintah dari client
                valread = read(new_socket, buffer, 1024);
                strncpy(command, buffer, valread);
    
                // Memproses perintah
                if (strncmp(command, "CREATE DATABASE", 15) == 0) {
                    char databaseName[50];
                    sscanf(command, "CREATE DATABASE %s;", databaseName);
                    createDatabase(databaseName);
                } else if (strncmp(command, "CREATE TABLE", 12) == 0) {
                    char databaseName[50];
                    char tableName[50];
                    char columns[200];
                    sscanf(command, "CREATE TABLE %s (%[^;]s);", tableName, columns);
                    char* token = strtok(tableName, ".");
                    strcpy(databaseName, token);
                    token = strtok(NULL, ".");
                    strcpy(tableName, token);
                    createTable(databaseName, tableName, columns);
                } else if (strncmp(command, "DROP DATABASE", 13) == 0) {
                    char databaseName[50];
                    sscanf(command, "DROP DATABASE %s;", databaseName);
                    dropDatabase(databaseName);
                } else if (strncmp(command, "DROP TABLE", 10) == 0) {
                    char databaseName[50];
                    char tableName[50];
                    sscanf(command, "DROP TABLE %s;", tableName);
                    char* token = strtok(tableName, ".");
                    strcpy(databaseName, token);
                    token = strtok(NULL, ".");
                    strcpy(tableName, token);
                    dropTable(databaseName, tableName);
                } else if (strncmp(command, "DROP COLUMN", 11) == 0) {
                    char databaseName[50];
                    char tableName[50];
                    char columnName[50];
                    sscanf(command, "DROP COLUMN %s FROM %s;", columnName, tableName);
                    char* token = strtok(tableName, ".");
                    strcpy(databaseName, token);
                    token = strtok(NULL, ".");
                    strcpy(tableName, token);
                    dropColumn(databaseName, tableName, columnName);
                } else if (strncmp(command, "INSERT INTO", 11) == 0) {
                    char databaseName[50];
                    char tableName[50];
                    char values[200];
                    sscanf(command, "INSERT INTO %[^.].%[^ ] %[^\n]", databaseName, tableName, values);
                    insertRow(databaseName, tableName, values);
                } else if (strncmp(command, "UPDATE", 6) == 0) {
                    char databaseName[50];
                    char tableName[50];
                    char columnName[50];
                    char newValue[50];
                    sscanf(command, "UPDATE %[^.].%[^ ] SET %[^=]=%s;", databaseName, tableName, columnName, newValue);
                    updateColumnValue(databaseName, tableName, columnName, newValue);
                } else if (strncmp(command, "DELETE FROM", 11) == 0) {
                    char databaseName[50];
                    char tableName[50];
                    sscanf(command, "DELETE FROM %[^.].%s;", databaseName, tableName);
                    deleteRows(databaseName, tableName);
                } else if (strncmp(command, "SELECT * FROM", 13) == 0) {
                    char databaseName[50];
                    char tableName[50];
                    sscanf(command, "SELECT * FROM %[^.].%s;", databaseName, tableName);
                    selectAll(databaseName, tableName, new_socket);
                } else if (strncmp(command, "SELECT", 6) == 0) {
    		      char databaseName[50];
    		      char tableName[50];
    		      char columnName[50];
    		      sscanf(command, "SELECT %[^\n] FROM %[^.].%[^ ]", databaseName, tableName, columnName);
    		      displayColumnData(databaseName, tableName, columnName);
                } else if (strncmp(command, "EXIT", 4) == 0) {
                    break;
                } else {
                    printf("Perintah tidak valid.\n");
                }
    ```
    
    - Perintah yang dikirim oleh klien akan diterima menggunakan fungsi **`read()`** dan disimpan dalam buffer **`buffer`**.
    - Isi buffer akan disalin ke variabel **`command`** menggunakan fungsi **`strncpy()`**.
    - Perintah yang diterima akan dibandingkan dengan berbagai perintah yang valid menggunakan fungsi **`strncmp()`** untuk menentukan tindakan yang harus dilakukan.
    - Berdasarkan jenis perintah yang diterima, argumen yang diperlukan akan diekstrak menggunakan fungsi **`sscanf()`**.
    - Fungsi-fungsi operasi database seperti **`createDatabase()`**, **`createTable()`**, **`dropDatabase()`**, **`dropTable()`**, **`dropColumn()`**, **`insertRow()`**, **`updateColumnValue()`**, **`deleteRows()`**, **`selectAll()`**, dan **`displayColumnData()`** akan dipanggil sesuai dengan jenis perintah yang diterima.
7. Mengirim Status ke Client
    
    ```c
    // Mengirimkan status ke client
                char response[1024];
                sprintf(response, "Perintah berhasil diproses.\n");
                send(new_socket, response, strlen(response), 0);
            }
    
            close(new_socket);
        }
    
        return 0;
    }
    ```
    
    - Setelah memproses perintah, server akan mengirimkan status ke klien menggunakan fungsi **`send()`**. Status tersebut akan memberi tahu klien apakah perintah berhasil diproses atau tidak.
    - Setelah selesai memproses perintah, soket untuk koneksi saat ini akan ditutup menggunakan fungsi **`close()`**.

## Output database.c

CREATE DATABASE demo
<a href="https://ibb.co/MVz4r5q"><img src="https://i.ibb.co/0M0STVP/Screenshot-2023-06-24-201220.png" alt="Screenshot-2023-06-24-201220" border="0"></a>

CREATE TABLE demo.fp (id INT, nama STRING, umur INT)
<a href="https://ibb.co/RBCKvLk"><img src="https://i.ibb.co/0FfLDPR/Screenshot-2023-06-24-201913.png" alt="Screenshot-2023-06-24-201913" border="0"></a>

DROP TABLE demo.fp 
**berhasil terhapus**
<a href="https://ibb.co/nR5xbLb"><img src="https://i.ibb.co/N7Hcsys/Screenshot-2023-06-24-220218.png" alt="Screenshot-2023-06-24-220218" border="0"></a>

DROP DATABASE demo 
**berhasil terhapus**
<a href="https://ibb.co/1LGt2JB"><img src="https://i.ibb.co/DtfHKgm/Screenshot-2023-06-24-220357.png" alt="Screenshot-2023-06-24-220357" border="0"></a>

DROP COLUMN id INT FROM demo.fp
<a href="https://ibb.co/fkRM1Mk"><img src="https://i.ibb.co/hB5s7sB/Screenshot-2023-06-24-202144.png" alt="Screenshot-2023-06-24-202144" border="0"></a>

INSERT INTO demo.fp (1,2,3)
INSERT INTO demo.fp (haha, hihi, huhu)
<a href="https://ibb.co/LJB6MTR"><img src="https://i.ibb.co/nn4fTXm/Screenshot-2023-06-24-202353.png" alt="Screenshot-2023-06-24-202353" border="0"></a>

UPDATE demo.fp SET id INT=update
<a href="https://ibb.co/M8989WQ"><img src="https://i.ibb.co/N1717Qb/Screenshot-2023-06-24-202657.png" alt="Screenshot-2023-06-24-202657" border="0"></a>

DELETE FROM demo.fp 
<a href="https://ibb.co/xFPBMD8"><img src="https://i.ibb.co/y4GZ8XB/Screenshot-2023-06-24-202814.png" alt="Screenshot-2023-06-24-202814" border="0"></a>

SELECT * FROM demo.fp
SELECT id INT, nama STRING FROM demo.fp
<img src="https://i.ibb.co/18NM166/Screenshot-2023-06-24-214048.png" alt="Screenshot-2023-06-24-214048" border="0">

<a href="https://ibb.co/yPtjVsF"><img src="https://i.ibb.co/2hx45K3/Screenshot-2023-06-24-213858.png" alt="Screenshot-2023-06-24-213858" border="0"></a>


<a href="https://ibb.co/Trkk8pj"><img src="https://i.ibb.co/hCyyWhQ/Whats-App-Image-2023-06-19-at-17-30-01.jpg" alt="Whats-App-Image-2023-06-19-at-17-30-01" border="0"></a>

**_yang berjuang satu orang doang nih mas/mba, kasihanilah sy :D_**





